const std = @import("std");
const testing = std.testing;
const assert = std.debug.assert;
const File = std.os.File;
const mem = std.mem;

pub const Adler32 = struct {
    const MOD: u32 = 65521;
    a: u64,
    b: u64,

    pub fn init() Adler32 {
        return Adler32{ .a = 1, .b = 0 };
    }

    pub fn reset(self: *Adler32) void {
        self.a = 1;
        self.b = 0;
    }

    pub fn update(self: *Adler32, data: []const u8) u32 {
        for (data) |byte| {
            _ = @addWithOverflow(u64, self.a, byte, &self.a); // a = a + byte;
            _ = @addWithOverflow(u64, self.b, self.a, &self.b); // b = b + a;
        }
        self.a = @mod(self.a, MOD);
        self.b = @mod(self.b, MOD);

        return self.checksum();
    }

    pub fn checksum(self: *Adler32) u32 {
        return @intCast(u32, (self.b << 16) | self.a);
    }

    pub fn compute(data: []const u8) u32 {
        var tmp = Adler32.init();
        return tmp.update(data);
    }
};

test "Adler32.compute" {
    assert(Adler32.compute("Wikipedia") == 300286872);
    assert(Adler32.compute("This is a longer line of text to perform the adler32 checksum on") == 0xe1ab16db);
}

test "Adler32.update" {
    var adler32 = Adler32.init();
    assert(adler32.update("Wikipedia") == 300286872);
    adler32.reset();
    assert(adler32.update("Wikipedia") == 300286872);
    assert(adler32.update("Wikipedia") == 0x441b072f);
    assert(adler32.checksum() == 0x441b072f);
}

pub const CRC32 = struct {
    v: u32,
    const table = comptime CRC32.gen_crc32_table();

    pub fn init() CRC32 {
        return CRC32{ .v = 0 };
    }

    pub fn reset(self: *CRC32) void {
        self.v = 0;
    }

    pub fn checksum(self: *CRC32) u32 {
        return self.v;
    }

    pub fn update(self: *CRC32, data: []const u8) u32 {
        var c = ~self.v;
        for (data) |byte| {
            c = CRC32.table[(c ^ byte) & 0xFF] ^ (c >> 8);
        }
        self.v = ~c;
        return self.v;
    }

    pub fn compute(data: []const u8) u32 {
        var crc = CRC32.init();
        return crc.update(data);
    }

    fn gen_crc32_table() [256]u32 {
        @setEvalBranchQuota(6000);
        comptime {
            var gtable: [256]u32 = undefined;
            var n: u32 = 0;
            while (n < gtable.len) : (n += 1) {
                var c: u32 = n;
                var k = 0;
                while (k < 8) : (k += 1) {
                    if ((c & 1) == 1) {
                        c = 0xedb88320 ^ (c >> 1);
                    } else {
                        c = c >> 1;
                    }
                }
                gtable[n] = c;
            }
            return gtable;
        }
    }
};

test "CRC32 Table Gen" {
    const table = CRC32.table;
    assert(table[0] == 0);
    assert(table[1] == 0x77073096);
    assert(table[255] == 0x2d02ef8d);
}

test "CRC32 Compute" {
    assert(CRC32.compute("Wikipedia") == 0xadaac02e);
    assert(CRC32.compute("This is a longer test of the CRC32 algorithm.") == 0x80a342b8);
}

test "CRC32 Update" {
    var crc = CRC32.init();
    assert(crc.update("Wikipedia") == 0xadaac02e);
    crc.reset();
    assert(crc.update("Wikipedia") == 0xadaac02e);
    assert(crc.update("Wikipedia") == 0xdb665038);
}

// This is the build out of the GZIP Headers

pub const GZipHeader = packed struct {
    magic1: u8, // == 0x1F
    magic2: u8, // == 0x8B

    cm: u8, // == 8
    flags: u8, // Check with the FLG_ bits
    mtime: u32, // unix mtime
    xflags: u8, // 2 = max compression, 4 = fastest algorithm
    os: u8, // os used to compress the file

    const FLG_FTEXT: u8 = 1;
    const FLG_FHCRC: u8 = 1 << 1;
    const FLG_FEXTRA: u8 = 1 << 2;
    const FLG_FNAME: u8 = 1 << 3;
    const FLG_FCOMMENT: u8 = 1 << 4;
};

pub fn gz_read_header(file: *File) !GZipHeader {
    try file.seekTo(0); // need to read from the start
    var header: GZipHeader = undefined;
    const bytes_read = file.read(@ptrCast([@sizeOf(GZipHeader)]u8, &header));

    return header;
}

test "read the gzip header" {
    var file = try File.openRead("c:/test.gz");
    var header = gz_read_header(&file);
}
