const std = @import("std");
const testing = std.testing;
const builtin = @import("builtin");

pub fn crc_builder(comptime T: type, comptime poly: T, comptime init: T, comptime refIn: bool, comptime refOut: bool, comptime xorOut: T, check: T) type {
    if (@typeInfo(T) != builtin.TypeId.Int or @typeInfo(T).Int.is_signed or T.bit_count < 8) @panic("crc_builder only allows unsigned integer of 8 bits or larger");

    return struct {
        crc: T,
        const table: [256]T = comptime __gen_table();
        const Self = @This();
        const hashsize = T.bit_count;

        fn init() Self {
            return Self{ .crc = init };
        }

        fn checksum(self: *const Self) T {
            return self.crc ^ xorOut;
        }

        fn update(self: *Self, bytes: []const u8) T {
            var c: T = if (refOut) @bitreverse(T, self.crc) else self.crc;

            if (refOut) {
                for (bytes) |byte| {
                    if (@sizeOf(T) > 1) {
                        c = Self.table[@intCast(usize, (c ^ byte) & 0xFF)] ^ (c >> 8);
                    } else {
                        c = Self.table[(c ^ byte) & 0xFF];
                    }
                }
            } else {
                const rs: T = if ((hashsize - 8) < 0) 0 else (hashsize - 8);
                for (bytes) |byte| {
                    if (@sizeOf(T) > 1) {
                        c = Self.table[@intCast(usize, ((c >> rs) ^ byte) & 0xFF)] ^ (c << 8);
                    } else {
                        c = Self.table[((c >> rs) ^ byte) & 0xFF];
                    }
                }
            }

            self.crc = @truncate(T, c);
            return @inlineCall(self.checksum);
        }

        fn reset(self: *Self) void {
            self.crc = init;
        }

        fn compute(bytes: []const u8) T {
            var crc = @inlineCall(Self.init);
            return crc.update(bytes);
        }

        fn validate(bytes: []const u8, value: T) bool {
            var crc = @inlineCall(Self.init);
            return (crc.update(bytes) == value);
        }

        fn __run_crc_check() void {
            testing.expectEqual(Self.init().update("123456789"), check);
        }

        fn __gen_table() [256]T {
            @setEvalBranchQuota(5000);

            var _table: [256]T = undefined;
            var n: usize = 0;
            while (n < _table.len) : (n += 1) {
                var c: T = @intCast(T, n);
                if (refIn) {
                    c = @bitreverse(T, c);
                } else if (hashsize > 8) {
                    c <<= (hashsize - 8);
                }

                const bitmask = (1 << (hashsize - 1));

                var k: u8 = 0;
                while (k < 8) : (k += 1) {
                    if ((c & bitmask) != 0) {
                        c = (c << 1) ^ poly;
                    } else {
                        c <<= 1;
                    }
                }
                if (refOut) c = @bitreverse(T, c);
                _table[n] = @truncate(T, c);
            }
            return _table;
        }
    };
}

/// Collection of CRC8 versions
/// Pulled from reveng.sourceforge.net/crc-catalogue/1-15.html
pub const CRC8 = crc_builder(u8, 0x07, 0x00, false, false, 0x00, 0xF4);
pub const CRC8H = crc_builder(u8, 0x2F, 0xFF, false, false, 0xFF, 0xDF);
pub const CRC8_AES = CRC8_EBU;
pub const CRC8_AUTOSAR = CRC8H;
pub const CRC8_BLUETOOTH = crc_builder(u8, 0xA7, 0x00, true, true, 0x00, 0x26);
pub const CRC8_CDMA2000 = crc_builder(u8, 0x9B, 0xFF, false, false, 0x00, 0xDA);
pub const CRC8_DARC = crc_builder(u8, 0x39, 0x00, true, true, 0x00, 0x15);
pub const CRC8_DOW = CRC8_MAXIM;
pub const CRC8_DVB_S2 = crc_builder(u8, 0xD5, 0x00, false, false, 0x00, 0xBC);
pub const CRC8_EBU = crc_builder(u8, 0x1D, 0xFF, true, true, 0x00, 0x97);
pub const CRC8_GSM_A = crc_builder(u8, 0x1D, 0x00, false, false, 0x00, 0x37);
pub const CRC8_GSM_B = crc_builder(u8, 0x49, 0x00, false, false, 0xFF, 0x94);
pub const CRC8_ICODE = crc_builder(u8, 0x1D, 0xFD, false, false, 0x00, 0x7E);
pub const CRC8_ITU = crc_builder(u8, 0x07, 0x00, false, false, 0x55, 0xA1);
pub const CRC8_J1850 = crc_builder(u8, 0x1D, 0xFF, false, false, 0xFF, 0x4B);
pub const CRC8_LTE = crc_builder(u8, 0x9B, 0x00, false, false, 0x00, 0xEA);
pub const CRC8_MAXIM = crc_builder(u8, 0x31, 0x00, true, true, 0x00, 0xA1);
pub const CRC8_MIFARE_MAD = crc_builder(u8, 0x1D, 0xC7, false, false, 0x00, 0x99);
pub const CRC8_NRSC_5 = crc_builder(u8, 0x31, 0xFF, false, false, 0x00, 0xF7);
pub const CRC8_OPENSAFTY = crc_builder(u8, 0x2F, 0x00, false, false, 0x00, 0x3E);
pub const CRC8_ROHC = crc_builder(u8, 0x07, 0xFF, true, true, 0x00, 0xD0);
pub const CRC8_SMBUS = crc_builder(u8, 0x07, 0x00, false, false, 0x00, 0xF4);
pub const CRC8_WCDMA = crc_builder(u8, 0x9B, 0x00, true, true, 0x00, 0x25);

test "CRC8 CRC Check Value" {
    CRC8.__run_crc_check();
    CRC8H.__run_crc_check();
    CRC8_AES.__run_crc_check();
    CRC8_AUTOSAR.__run_crc_check();
    CRC8_BLUETOOTH.__run_crc_check();
    CRC8_CDMA2000.__run_crc_check();
    CRC8_DARC.__run_crc_check();
    CRC8_DOW.__run_crc_check();
    CRC8_DVB_S2.__run_crc_check();
    CRC8_EBU.__run_crc_check();
    CRC8_GSM_A.__run_crc_check();
    CRC8_GSM_B.__run_crc_check();
    CRC8_ICODE.__run_crc_check();
    CRC8_ITU.__run_crc_check();
    CRC8_J1850.__run_crc_check();
    CRC8_LTE.__run_crc_check();
    CRC8_MAXIM.__run_crc_check();
    CRC8_MIFARE_MAD.__run_crc_check();
    CRC8_NRSC_5.__run_crc_check();
    CRC8_OPENSAFTY.__run_crc_check();
    CRC8_ROHC.__run_crc_check();
    CRC8_SMBUS.__run_crc_check();
    CRC8_WCDMA.__run_crc_check();
}

test "CRC8 Usage Check" {
    testing.expectEqual(u8(0xC1), CRC8.compute("The quick brown fox jumps over the lazy dog"));
    testing.expect(CRC8.validate("The quick brown fox jumps over the lazy dog", 0xC1));
    testing.expect(!CRC8.validate("The quick brown fox jumps over the lazy dog", 0xC2));

    var crc8 = CRC8.init();
    testing.expectEqual(u8(0x00), crc8.checksum());
    testing.expectEqual(u8(0xC1), crc8.update("The quick brown fox jumps over the lazy dog"));
    crc8.reset();
    testing.expectEqual(u8(0x00), crc8.checksum());
    testing.expectEqual(u8(0xC1), crc8.update("The quick brown fox jumps over the lazy dog"));
    testing.expectEqual(u8(0x84), crc8.update("All Over the Moon!"));
    testing.expectEqual(u8(0x00), crc8.update([]u8{0x84}));

    testing.expectEqual(u8(0xEF), CRC8.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x3D), CRC8_CDMA2000.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0xF7), CRC8_DARC.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x02), CRC8_DVB_S2.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x75), CRC8_EBU.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x0C), CRC8_ICODE.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0xBA), CRC8_ITU.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0xF4), CRC8_MAXIM.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x32), CRC8_ROHC.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x9B), CRC8_WCDMA.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0xCB), CRC8_J1850.compute([]u8{ 0x33, 0x22, 0x55, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff }));
    testing.expectEqual(u8(0xE9), CRC8_J1850.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x11), CRC8H.compute([]u8{ 0x33, 0x22, 0x55, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff }));
    testing.expectEqual(u8(0x40), CRC8H.compute("ZIG IS GREAT!"));
    testing.expectEqual(u8(0x40), CRC8_AUTOSAR.compute("ZIG IS GREAT!"));
}

// CRC10

/// Pulled from reveng.sourceforge.net/crc-catalogue/1-15.html
pub const CRC10 = CRC10_ATM;
pub const CRC10_ATM = crc_builder(u10, 0x233, 0x000, false, false, 0x000, 0x199);
pub const CRC10_CDMA2000 = crc_builder(u10, 0x3D9, 0x3FF, false, false, 0x000, 0x233);
pub const CRC10_GSM = crc_builder(u10, 0x175, 0x000, false, false, 0x3ff, 0x12A);

test "CRC10 CRC Check Value" {
    CRC10.__run_crc_check();
    CRC10_ATM.__run_crc_check();
    CRC10_CDMA2000.__run_crc_check();
    CRC10_GSM.__run_crc_check();
}

/// CRC16
/// Pulled from reveng.sourceforge.net/crc-catalogue/16.html
const CRC16 = crc_builder(u16, 0x8005, 0x0000, true, true, 0x0000, 0xBB3D);
const CRC16A = CRC16_ISO_IEC_14443_3_A;
const CRC16B = CRC16_ISO_IEC_14443_3_B;
const CRC16R = CRC16_DECT_R;
const CRC16X = CRC16_DECT_X;
const CRC16_ACORN = CRC16_XMODEM;
const CRC16_ARC = CRC16;
const CRC16_AUG_CCITT = CRC16_SPI_FUJITSU;
const CRC16_AUTOSAR = CRC16_IBM_3740;
const CRC16_BUYPASS = CRC16_UMTS;
const CRC16_CCITT = CRC16_KERMIT;
const CRC16_CCITT_TRUE = CRC16_CCITT;
const CRC16_CCITT_FALSE = CRC16_IBM_3740;
const CRC16_CDMA2000 = crc_builder(u16, 0xC867, 0xFFFF, false, false, 0x0000, 0x4C06);
const CRC16_CMS = crc_builder(u16, 0x8005, 0xFFFF, false, false, 0x0000, 0xAEE7);
const CRC16_DARC = CRC16_GENIBUS;
const CRC16_DDS_110 = crc_builder(u16, 0x8005, 0x800D, false, false, 0x0000, 0x9ECF);
const CRC16_DECT_R = crc_builder(u16, 0x0589, 0x0000, false, false, 0x0001, 0x007E);
const CRC16_DECT_X = crc_builder(u16, 0x0589, 0x0000, false, false, 0x0000, 0x007f);
const CRC16_DNP = crc_builder(u16, 0x3D65, 0x0000, true, true, 0xFFFF, 0xEA82);
const CRC16_EN_13757 = crc_builder(u16, 0x3D65, 0x0000, false, false, 0xFFFF, 0xC2B7);
const CRC16_EPC = CRC16_GENIBUS;
const CRC16_GENIBUS = crc_builder(u16, 0x1021, 0xFFFF, false, false, 0xFFFF, 0xD64E);
const CRC16_GSM = crc_builder(u16, 0x1021, 0x0000, false, false, 0xFFFF, 0xCE3C);
const CRC16_IBM = CRC16;
const CRC16_IBM_3740 = crc_builder(u16, 0x1021, 0xFFFF, false, false, 0x0000, 0x29B1);
const CRC16_IBM_SDLC = crc_builder(u16, 0x1021, 0xFFFF, true, true, 0xFFFF, 0x906E);
const CRC16_ICODE = CRC16_GENIBUS;
const CRC16_IEC_61158_2 = CRC16_PROFIBUS;
const CRC16_ISO_IEC_14443_3_A = crc_builder(u16, 0x1021, 0xC6C6, true, true, 0x0000, 0xBF05);
const CRC16_ISO_IEC_14443_3_B = CRC16_ISO_HDLC;
const CRC16_ISO_HDLC = CRC16_IBM_SDLC;
const CRC16_KERMIT = crc_builder(u16, 0x1021, 0x0000, true, true, 0x0000, 0x2189);
const CRC16_LHA = CRC16;
const CRC16_LJ1200 = crc_builder(u16, 0x6F63, 0x0000, false, false, 0x0000, 0xBDF4);
const CRC16_LTE = CRC16_XMODEM;
const CRC16_MAXIM = crc_builder(u16, 0x8005, 0x0000, true, true, 0xFFFF, 0x44C2);
const CRC16_MCRF4XX = crc_builder(u16, 0x1021, 0xFFFF, true, true, 0x0000, 0x6F91);
const CRC16_MODBUS = crc_builder(u16, 0x8005, 0xFFFF, true, true, 0x0000, 0x4B37);
const CRC16_NRSC5 = crc_builder(u16, 0x080B, 0xFFFF, true, true, 0x0000, 0xA066);
const CRC16_OPENSAFTY_A = crc_builder(u16, 0x5935, 0x0000, false, false, 0x0000, 0x5D38);
const CRC16_OPENSAFTY_B = crc_builder(u16, 0x755B, 0x0000, false, false, 0x0000, 0x20FE);
const CRC16_PROFIBUS = crc_builder(u16, 0x1DCF, 0xFFFF, false, false, 0xFFFF, 0xA819);
const CRC16_RIELLO = crc_builder(u16, 0x1021, 0xB2AA, true, true, 0x0000, 0x63D0);
const CRC16_SPI_FUJITSU = crc_builder(u16, 0x1021, 0x1D0F, false, false, 0x0000, 0xE5CC);
const CRC16_T10_DIF = crc_builder(u16, 0x8BB7, 0x0000, false, false, 0x0000, 0xD0DB);
const CRC16_TELEDISK = crc_builder(u16, 0xA097, 0x0000, false, false, 0x0000, 0x0FB3);
const CRC16_TMS37157 = crc_builder(u16, 0x1021, 0x89EC, true, true, 0x0000, 0x26B1);
const CRC16_UMTS = crc_builder(u16, 0x8005, 0x0000, false, false, 0x0000, 0xFEE8);
const CRC16_USB = crc_builder(u16, 0x8005, 0xFFFF, true, true, 0xFFFF, 0xB4C8);
const CRC16_V_41_LSB = CRC16_CCITT;
const CRC16_V_41_MSB = CRC16_XMODEM;
const CRC16_VERIFONE = CRC16_UMTS;
const CRC16_X25 = CRC16_IBM_SDLC;
const CRC16_XMODEM = crc_builder(u16, 0x1021, 0x0000, false, false, 0x0000, 0x31C3);
const CRC16_ZMODEM = CRC16_XMODEM;

test "CRC16 CRC Check Value" {
    CRC16.__run_crc_check();
    CRC16A.__run_crc_check();
    CRC16B.__run_crc_check();
    CRC16R.__run_crc_check();
    CRC16X.__run_crc_check();
    CRC16_ACORN.__run_crc_check();
    CRC16_ARC.__run_crc_check();
    CRC16_AUG_CCITT.__run_crc_check();
    CRC16_AUTOSAR.__run_crc_check();
    CRC16_BUYPASS.__run_crc_check();
    CRC16_CCITT.__run_crc_check();
    CRC16_CCITT_TRUE.__run_crc_check();
    CRC16_CCITT_FALSE.__run_crc_check();
    CRC16_CDMA2000.__run_crc_check();
    CRC16_CMS.__run_crc_check();
    CRC16_DARC.__run_crc_check();
    CRC16_DDS_110.__run_crc_check();
    CRC16_DECT_R.__run_crc_check();
    CRC16_DECT_X.__run_crc_check();
    CRC16_DNP.__run_crc_check();
    CRC16_EN_13757.__run_crc_check();
    CRC16_EPC.__run_crc_check();
    CRC16_GENIBUS.__run_crc_check();
    CRC16_GSM.__run_crc_check();
    CRC16_IBM.__run_crc_check();
    CRC16_IBM_3740.__run_crc_check();
    CRC16_IBM_SDLC.__run_crc_check();
    CRC16_ICODE.__run_crc_check();
    CRC16_IEC_61158_2.__run_crc_check();
    CRC16_ISO_IEC_14443_3_A.__run_crc_check();
    CRC16_ISO_IEC_14443_3_B.__run_crc_check();
    CRC16_ISO_HDLC.__run_crc_check();
    CRC16_KERMIT.__run_crc_check();
    CRC16_LHA.__run_crc_check();
    CRC16_LJ1200.__run_crc_check();
    CRC16_LTE.__run_crc_check();
    CRC16_MAXIM.__run_crc_check();
    CRC16_MCRF4XX.__run_crc_check();
    CRC16_MODBUS.__run_crc_check();
    CRC16_NRSC5.__run_crc_check();
    CRC16_OPENSAFTY_A.__run_crc_check();
    CRC16_OPENSAFTY_B.__run_crc_check();
    CRC16_PROFIBUS.__run_crc_check();
    CRC16_RIELLO.__run_crc_check();
    CRC16_SPI_FUJITSU.__run_crc_check();
    CRC16_T10_DIF.__run_crc_check();
    CRC16_TELEDISK.__run_crc_check();
    CRC16_TMS37157.__run_crc_check();
    CRC16_UMTS.__run_crc_check();
    CRC16_USB.__run_crc_check();
    CRC16_V_41_LSB.__run_crc_check();
    CRC16_V_41_MSB.__run_crc_check();
    CRC16_VERIFONE.__run_crc_check();
    CRC16_X25.__run_crc_check();
    CRC16_XMODEM.__run_crc_check();
    CRC16_ZMODEM.__run_crc_check();
}

/// CRC17
const CRC17_CAN_FD = crc_builder(u17, 0x1685B, 0x00000, false, false, 0x00000, 0x04F03);

/// CRC21
const CRC21_CAN_FD = crc_builder(u21, 0x102899, 0x000000, false, false, 0x000000, 0x0ED841);

test "CRC17 and CRC21 CRC Value Check" {
    CRC17_CAN_FD.__run_crc_check();
    CRC21_CAN_FD.__run_crc_check();
}

/// CRC24
/// Pulled from reveng.sourceforge.net/crc-catalogue/17.html
const CRC24 = CRC24_OPENPGP;
const CRC24_BLE = crc_builder(u24, 0x00065B, 0x555555, true, true, 0x000000, 0xC25A56);
const CRC24_FLEXRAY_A = crc_builder(u24, 0x5D6DCB, 0xFEDCBA, false, false, 0x000000, 0x7979BD);
const CRC24_FLEXRAY_B = crc_builder(u24, 0x5D6DCB, 0xABCDEF, false, false, 0x000000, 0x1F23B8);
const CRC24_INTERLAKEN = crc_builder(u24, 0x328B63, 0xFFFFFF, false, false, 0xFFFFFF, 0xB4F3E6);
const CRC24_LTE_A = crc_builder(u24, 0x864CFB, 0x000000, false, false, 0x000000, 0xCDE703);
const CRC24_LTE_B = crc_builder(u24, 0x800063, 0x000000, false, false, 0x000000, 0x23EF52);
const CRC24_OPENPGP = crc_builder(u24, 0x864CFB, 0xB704CE, false, false, 0x000000, 0x21CF02);
const CRC24_OS_9 = crc_builder(u24, 0x800063, 0xFFFFFF, false, false, 0xFFFFFF, 0x200FA5);

const CRC30_CDMA = crc_builder(u30, 0x2030B9C7, 0x3FFFFFFF, false, false, 0x3FFFFFFF, 0x04C34ABF);
const CRC31_PHILIPS = crc_builder(u31, 0x04C11DB7, 0x7FFFFFFF, false, false, 0x7FFFFFFF, 0x0CE9E46C);

test "CRC24 CRC30 CRC31 CRC Check Value" {
    CRC24.__run_crc_check();
    CRC24_BLE.__run_crc_check();
    CRC24_FLEXRAY_A.__run_crc_check();
    CRC24_FLEXRAY_B.__run_crc_check();
    CRC24_INTERLAKEN.__run_crc_check();
    CRC24_LTE_A.__run_crc_check();
    CRC24_LTE_B.__run_crc_check();
    CRC24_OPENPGP.__run_crc_check();
    CRC24_OS_9.__run_crc_check();
    CRC30_CDMA.__run_crc_check();
    CRC31_PHILIPS.__run_crc_check();
}

/// CRC32
/// Pulled from reveng.sourceforge.net/crc-catalogue/17.html
const CRC32 = CRC32_ISO_HDLC;
const CRC32B = CRC32_BZIP2;
const CRC32C = CRC32_ISCSI;
const CRC32D = CRC32_BASE91_D;
const CRC32Q = CRC32_AIXM;
const CRC32_AAL5 = CRC32_BZIP2;
const CRC32_ADCCP = CRC32_ISO_HDLC;
const CRC32_AIXM = crc_builder(u32, 0x814141AB, 0x00000000, false, false, 0x00000000, 0x3010BF7F);
const CRC32_AUTOSAR = crc_builder(u32, 0xF4ACFB13, 0xFFFFFFFF, true, true, 0xFFFFFFFF, 0x1697D06A);
const CRC32_BASE91_C = CRC32_ISCSI;
const CRC32_BASE91_D = crc_builder(u32, 0xA833982B, 0xFFFFFFFF, true, true, 0xFFFFFFFF, 0x87315576);
const CRC32_BZIP2 = crc_builder(u32, 0x4C11DB7, 0xFFFFFFFF, false, false, 0xFFFFFFFF, 0xFC891918);
const CRC32_CASTAGNOLI = CRC32_ISCSI;
const CRC32_CKSUM = crc_builder(u32, 0x04C11DB7, 0x00000000, false, false, 0xFFFFFFFF, 0x765E7680);
const CRC32_DECT_B = CRC32_BZIP2;
const CRC32_INTERLAKEN = CRC32_ISCSI;
const CRC32_ISCSI = crc_builder(u32, 0x1EDC6F41, 0xFFFFFFFF, true, true, 0xFFFFFFFF, 0xE3069283);
const CRC32_ISO_HDLC = crc_builder(u32, 0x04C11DB7, 0xFFFFFFFF, true, true, 0xFFFFFFFF, 0xCBF43926);
const CRC32_JAMCRC = crc_builder(u32, 0x04C11DB7, 0xFFFFFFFF, true, true, 0x00000000, 0x340BC6D9);
const CRC32_MPEG_2 = crc_builder(u32, 0x04C11DB7, 0xFFFFFFFF, false, false, 0x00000000, 0x0376E6E7);
const CRC32_PKZIP = CRC32_ISO_HDLC;
const CRC32_POSIX = CRC32_CKSUM;
const CRC32_V42 = CRC32_ISO_HDLC;
const CRC32_XFER = crc_builder(u32, 0x000000AF, 0x00000000, false, false, 0x00000000, 0xBD0BE338);
const CRC32_XY = CRC32_ISO_HDLC;

const CRC40_GSM = crc_builder(u40, 0x0004820009, 0x0000000000, false, false, 0xFFFFFFFFFF, 0xD4164FC646);

test "CRC32 CRC Check Value" {
    CRC32.__run_crc_check();
    CRC32B.__run_crc_check();
    CRC32C.__run_crc_check();
    CRC32D.__run_crc_check();
    CRC32Q.__run_crc_check();
    CRC32_AAL5.__run_crc_check();
    CRC32_ADCCP.__run_crc_check();
    CRC32_AIXM.__run_crc_check();
    CRC32_AUTOSAR.__run_crc_check();
    CRC32_BASE91_C.__run_crc_check();
    CRC32_BASE91_D.__run_crc_check();
    CRC32_BZIP2.__run_crc_check();
    CRC32_CASTAGNOLI.__run_crc_check();
    CRC32_CKSUM.__run_crc_check();
    CRC32_DECT_B.__run_crc_check();
    CRC32_INTERLAKEN.__run_crc_check();
    CRC32_ISCSI.__run_crc_check();
    CRC32_ISO_HDLC.__run_crc_check();
    CRC32_JAMCRC.__run_crc_check();
    CRC32_MPEG_2.__run_crc_check();
    CRC32_PKZIP.__run_crc_check();
    CRC32_POSIX.__run_crc_check();
    CRC32_V42.__run_crc_check();
    CRC32_XFER.__run_crc_check();
    CRC32_XY.__run_crc_check();
    CRC40_GSM.__run_crc_check();
}

/// CRC64
/// Pulled from reveng.sourceforge.net/crc-catalogue/17.html
const CRC64 = CRC64_ECMA_182;
const CRC64_ECMA_182 = crc_builder(u64, 0x42F0E1EBA9EA3693, 0x0000000000000000, false, false, 0x0000000000000000, 0x6C40DF5F0B497347);
const CRC64_GO_ISO = crc_builder(u64, 0x000000000000001B, 0xFFFFFFFFFFFFFFFF, true, true, 0xFFFFFFFFFFFFFFFF, 0xB90956C775A41001);
const CRC64_GO_ECMA = CRC64_XZ;
const CRC64_WE = crc_builder(u64, 0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, false, false, 0xFFFFFFFFFFFFFFFF, 0x62EC59E3F1a4F00A);
const CRC64_XZ = crc_builder(u64, 0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, true, true, 0xFFFFFFFFFFFFFFFF, 0x995DC9BBDF1939FA);

test "CRC64 CRC Check Value" {
    CRC64.__run_crc_check();
    CRC64_ECMA_182.__run_crc_check();
    CRC64_GO_ISO.__run_crc_check();
    CRC64_GO_ECMA.__run_crc_check();
    CRC64_WE.__run_crc_check();
    CRC64_XZ.__run_crc_check();
}

/// CRC82
/// Pulled from reveng.sourceforge.net/crc-catalogue/17.html
const CRC82_DARC = crc_builder(u82, 0x0308C0111011401440411, 0x000000000000000000000, true, true, 0x000000000000000000000, 0x09EA83F625023801FD612);

test "CRC82 CRC Check Value" {
    CRC82_DARC.__run_crc_check();
}

/// This is the old Unix Sysem-V sum crc
/// This is not a good CRC, only included as its still around
///
pub const CRC_SYSV = struct {
    crc: u32,
    const Self = @This();

    fn init() CRC_SYSV {
        return CRC_SYSV{ .crc = 0 };
    }

    fn checksum(self: *const Self) u32 {
        return self.crc;
    }

    fn update(self: *Self, bytes: []const u8) u32 {
        for (bytes) |byte| {
            _ = @addWithOverflow(u32, self.crc, byte, &self.crc);
        }
        return self.crc;
    }

    fn reset(self: *Self) void {
        self.crc = 0;
    }

    fn compute(bytes: []const u8) u32 {
        var crc = Self.init();
        return crc.update(bytes);
    }

    fn validate(bytes: []const u8, value: u32) bool {
        return Self.compute(bytes) == value;
    }
};

test "CRC_SYSV" {
    // This is the same as doing the following
    // echo "123456789" | sum -s
    testing.expectEqual(CRC_SYSV.compute("123456789\n"), 487);
    testing.expect(CRC_SYSV.validate("123456789\n", 487));
    testing.expect(!CRC_SYSV.validate("0123456789\n", 487));
}

/// This is the BSD sum crc
/// This is not a good CRC, only included as its still around
///
pub const CRC_BSD = struct {
    crc: u32,
    const Self = @This();

    fn init() CRC_BSD {
        return CRC_BSD{ .crc = 0 };
    }

    fn checksum(self: *const Self) u32 {
        return self.crc;
    }

    fn update(self: *Self, bytes: []const u8) u32 {
        var c = self.crc;
        for (bytes) |byte| {
            c = (c >> 1) + ((c & 1) << 15);
            c += byte;
            c &= 0xFFFF;
        }
        self.crc = c;
        return self.crc;
    }

    fn reset(self: *Self) void {
        self.crc = 0;
    }

    fn compute(bytes: []const u8) u32 {
        var crc = Self.init();
        return crc.update(bytes);
    }

    fn validate(bytes: []const u8, value: u32) bool {
        return Self.compute(bytes) == value;
    }
};

test "CRC_BSD" {
    // This is the same as doing the following on unix systems
    // echo "123456789" | sum -r
    testing.expectEqual(CRC_BSD.compute("123456789\n"), 59585);
    testing.expect(CRC_BSD.validate("123456789\n", 59585));
    testing.expect(!CRC_BSD.validate("0123456789\n", 59585));
}
